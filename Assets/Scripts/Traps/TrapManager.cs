using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class TrapManager : LocalSingleton<TrapManager>
{
    public static string playerColor;
    string trapTag;
    
    private void Awake()
    {
        
        trapTag = gameObject.tag;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")&& playerColor == trapTag || PlayerMover.Instance.powerUp)
        {
            if(!PlayerMover.Instance.powerUp)
                PlayerMover.Instance.UpSpeed();
           GameObject brokenObj = Instantiate(LevelManager.Instance.brokenTrap,
                new Vector3(LevelManager.Instance.brokenTrap.transform.position.x,
                    LevelManager.Instance.brokenTrap.transform.position.y, transform.position.z), Quaternion.identity);
            Destroy(gameObject.transform.parent.gameObject);
            Destroy(brokenObj,.5f);
        }
        else
        {
            if (!PlayerMover.Instance.powerUp)
            {
                PlayerMover.Instance.DownSpeed();
            }
        }
    }
}
