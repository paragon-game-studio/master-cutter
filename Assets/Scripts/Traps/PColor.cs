using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PColor : LocalSingleton<PColor>
{
    SkinnedMeshRenderer renderer;
    Material[] color;
  
    string colorTag;
    private void Awake()
    {
        renderer = GetComponent<SkinnedMeshRenderer>();
        renderer.enabled = true;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            PlayerMover.onFinish = true;
            Level.Instance.NextLevel();
        }
        if (!other.CompareTag("Trap"))
        {
            colorTag = other.gameObject.tag;
        }
   
        if (other.CompareTag(colorTag) && other.gameObject.layer==6)
        {
            color =new Material[] {other.gameObject.GetComponent<MeshRenderer>().materials[0],other.gameObject.GetComponent<MeshRenderer>().materials[1]};
            renderer.sharedMaterial = color[0];
            GameObject.Find("Ps").GetComponent<MeshRenderer>().sharedMaterial = color[0];
            GameObject.Find("Ps2").GetComponent<MeshRenderer>().sharedMaterial = color[0];
            GameObject.Find("Slice").GetComponent<MeshRenderer>().sharedMaterial = color[0];
            GameObject glas= Instantiate(LevelManager.Instance.glass, other.transform.position, Quaternion.identity);
            CurrentCollor.Instance.ColorChange(color[1]);
            glas.transform.localScale=Vector3.one;
            Destroy(other.gameObject,.2f);
            Destroy(glas,1);
        }
      

    }
}
