using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerControl : MonoBehaviour
{
    string wallColor;
    private void Awake()
    {
        wallColor = gameObject.tag;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && gameObject.tag == wallColor)
        {
            TrapManager.playerColor = wallColor;
        }
    }
}
