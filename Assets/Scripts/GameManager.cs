using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : LocalSingleton<GameManager>
{
    public GameObject _bar;
    public GameObject unlem;
    public Image _poverBar;
    public GameObject fX;
    public float distance;
    public bool _poverUpBool;
    public bool _barFull = false;
    bool unlemBool = false;

    // Time
    public float Invertal;
    private void Awake()
    {
        _bar.SetActive(false);
    }
    void Update()
    {
        _poverBar.fillAmount = distance;
        if (_poverUpBool && !_barFull)
        {
            Invertal += Time.deltaTime;
            if (Invertal > .5f)
            {
                _poverUpBool = false;
            }
            if (distance >= 1)
            {
                if (fX != null)
                    fX.SetActive(true);
                _poverBar.color = Color.red;

                _barFull = true;
                _poverUpBool = false;
                unlemBool = true;
            }
            else if (distance > 0)
            {
                _bar.SetActive(true);
            }

        }
        else if (!_poverUpBool || _barFull)
        {
            distance -= .0015f;
            if (distance < .30f && _barFull && unlemBool)
            {
                unlemBool = false;
                StartCoroutine(Unlem());
            }

            if (distance < .01)
            {
                distance = 0;
                if (fX != null)
                    fX.SetActive(false);
                _poverBar.color = Color.white;
                _barFull = false;

                _bar.SetActive(false);
            }

        }
    }
    IEnumerator Unlem()
    {
        while (true)
        {
            yield return new WaitForSeconds(.05f);
            if (_barFull)
            {
                unlem.SetActive(true);
                yield return new WaitForSeconds(.15f);
                unlem.SetActive(false);
                yield return new WaitForSeconds(.15f);
            }
            else
            {
                break;
            }
        }
    }
}
