using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMover : LocalSingleton<PlayerMover>
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private float turnSpeed = 5f;
    [SerializeField] private float cameraFollowZ = 5f;
    [SerializeField] private float cameraFollowSpeed = .1f;
    [SerializeField] private VariableJoystick joystick;
    public static bool onStart = false;
    public static bool onFinish = false;
    private int index = 0;

    [Header("==Power Up==")]
    public bool powerUp = false;
    public float poverTurnSpeed;
    public float poverTime;
    public bool powerBack;
    private void Start()
    {

        onStart = false;
        onFinish = false;
    }

    private void Update()
    {


        if (onStart)
        {
            if (speed<17)
            {
                transform.DOLocalRotate(new Vector3(0, 0, 0), .5f);
                transform.DOLocalMove(new Vector3(0,-3.2f,transform.position.z+4), .7f);
                onStart = false;
                LevelManager.Instance.PlayAnim(1);
                GameObject.Find("Traps").SetActive(false);
                GameObject.Find("Slice").transform.DOScale(Vector3.zero, .1f);
                GameObject.Find("Cloak").GetComponent<Animator>().enabled = false;
                LevelComplotedLose.Instance.loseB = true;
                Level.Instance.RetyLevel();
            }
            Move();
        }
    }

    void Move()
    {
<<<<<<< HEAD
        transform.Translate(transform.forward * Time.deltaTime * speed, Space.Self);
        transform.Rotate(new Vector3(0, 0, -Input.GetAxis("Horizontal") * turnSpeed * Time.deltaTime), Space.Self);
        transform.Rotate(new Vector3(0, 0, -joystick.Horizontal * turnSpeed * Time.deltaTime), Space.Self);
=======
        
        transform.Translate(transform.forward*Time.deltaTime*speed,Space.Self);
        transform.Rotate(new Vector3(0,0,-Input.GetAxis("Horizontal")*turnSpeed*Time.deltaTime),Space.Self);
        transform.Rotate(new Vector3(0,0,-joystick.Horizontal*turnSpeed*Time.deltaTime),Space.Self);
>>>>>>> 8f3b3cda2b7f9fe6693cb52eba8b640675409371
        if (!onFinish)
        {
            Camera.main.transform.DOMoveZ(transform.position.z + cameraFollowZ, cameraFollowSpeed);
        }
        if (GameManager.Instance._barFull)
        {
            PowerUp();
        }

    }

    public void DownSpeed()
    {
        index = 0;
        speed -= 1;
    }
    public void UpSpeed()
    {
<<<<<<< HEAD
        if (!GameManager.Instance._barFull)
        {
            GameManager.Instance.Invertal = 0;
            GameManager.Instance._poverUpBool = true;
            StartCoroutine(PoverBar());
        }
        if (speed < 28)
        {
            speed += 1;
        }
=======
        index++;
        if (speed<28)
        {
            speed += 1;
        }

        switch (index)
        {
            case 6:
                LevelManager.Instance.Sprite(0);
                break; 
            case 18:
                LevelManager.Instance.Sprite(1);
                break; 
            case 36:
                LevelManager.Instance.Sprite(2);
                break;
            case 54:
                LevelManager.Instance.Sprite(3);
                index = 0;
                break;
        }
>>>>>>> 8f3b3cda2b7f9fe6693cb52eba8b640675409371
    }
    void PowerUp()
    {
        powerUp = true;
        transform.Rotate(0, 0, poverTurnSpeed * Time.deltaTime);
    }
    IEnumerator PoverBar()
    {
        for (int i = 0; i < 15; i++)
        {
            yield return new WaitForSeconds(.01f);
            GameManager.Instance.distance += .0025f;
        }
    }
}
