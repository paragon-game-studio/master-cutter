using System.Collections;
using System.Collections.Generic;
using Animancer;
using DG.Tweening;
using UnityEngine;

public class LevelManager : LocalSingleton<LevelManager>
{
    [SerializeField] private AnimancerComponent superPlayer;
    [SerializeField] private AnimationClip fly, fall;
    [SerializeField] private GameObject floorPrefab, holePrefab;
    [SerializeField] private Transform floors;
    [SerializeField] private Transform camera;
    public GameObject brokenTrap, glass; 
    GameObject good, waouh, avasome, amazing;

    protected override void Awake()
    {
        good=GameObject.Find("Good");
        waouh=GameObject.Find("Waouh");
        avasome=GameObject.Find("Avasome");
        amazing=GameObject.Find("Amazing");
        
        base.Awake();
    }

    private void Start()
    {
        PlayAnim();
        // StartCoroutine(FloorControl());
    }

    public void PlayAnim(int index = 0)
    {
        switch (index)
        {
            case 0:
                superPlayer.Play(fly);
                break;
            case 1:
                superPlayer.Play(fall);
                break;
        }

    }

   public void Sprite(int index)
    {
        switch (index)
        {
            case 0:
                good.transform.DOScale(Vector3.one, .2f).SetEase(Ease.InBounce).OnComplete((() =>
                {
                    good.transform.DOScale(Vector3.zero, .2f).SetEase(Ease.InBack).SetDelay(.2f);
                }));
                break;
            case 1:
                waouh.transform.DOScale(Vector3.one, .2f).SetEase(Ease.InBounce).OnComplete((() =>
                {
                    waouh.transform.DOScale(Vector3.zero, .2f).SetEase(Ease.InBack).SetDelay(.2f);
                }));
                break;
            case 2:
                avasome.transform.DOScale(Vector3.one, .2f).SetEase(Ease.InBounce).OnComplete((() =>
                {
                    avasome.transform.DOScale(Vector3.zero, .2f).SetEase(Ease.InBack).SetDelay(.2f);
                }));
                break;
            case 3:
                amazing.transform.DOScale(Vector3.one, .2f).SetEase(Ease.InBounce).OnComplete((() =>
                {
                    amazing.transform.DOScale(Vector3.zero, .2f).SetEase(Ease.InBack).SetDelay(.2f);
                }));
                break;

        }

    }



    // IEnumerator FloorControl()
        //    {
        //        float lastSpawnPos = 5f;
        //        int lastSpawnedFloorId = 0;
        //        List<Transform> floorList = new List<Transform>();
        //        int i = 0;
        //        for (i = 0; i < 30; i++)
        //        {
        //            GameObject spawnedFloor;
        //            if (i != 7 )
        //            {
        //                spawnedFloor = Instantiate(floorPrefab, floors);
        //            }
        //            else
        //            {
        //                spawnedFloor = Instantiate(holePrefab, floors);
        //            }
        //            floorList.Add(spawnedFloor.transform);
        //            spawnedFloor.transform.position = new Vector3(0,-9.5f,i*5);
        //        }
        //        while (true)
        //        {
        //            if(camera.transform.position.z > lastSpawnPos)
        //            {
        //                lastSpawnPos = camera.transform.position.z + 5f;
        //                floorList[lastSpawnedFloorId%30].position = new Vector3(0, -9.5f, i * 5);
        //                i++;
        //                lastSpawnedFloorId++;
        //            }
        //            yield return new WaitForSeconds(0.01f);
        //        }
        //    }
    
}
