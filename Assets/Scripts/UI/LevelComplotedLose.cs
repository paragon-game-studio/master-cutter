using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class LevelComplotedLose : LocalSingleton<LevelComplotedLose>
{
    public GameObject LoseComploted;
    public GameObject lose;
    public GameObject comploted;
    public bool loseB =false, complotedB = false;
    public int yuzde;
    
    void Update()
    {
        if (complotedB)
        {
            comploted.SetActive(true);
            complotedB = false;
            LoseComploted.GetComponent<Image>().color = Color.green;
            comploted.transform.DOScale(Vector3.one, .2f).SetEase(Ease.Linear);
            lose.SetActive(false);
            StartCoroutine(LoseComplotedImage());
        }
        if (loseB)
        {
            lose.SetActive(true);
            loseB = false;
            LoseComploted.GetComponent<Image>().color = Color.red;
            lose.transform.DOScale(Vector3.one, .2f).SetEase(Ease.Linear);
            comploted.SetActive(false);
            
            StartCoroutine(LoseComplotedImage());
        }
    }
    IEnumerator LoseComplotedImage()
    {
        yield return new WaitForSeconds(.3f);
        comploted.transform.DOScale(Vector3.zero, .2f).SetDelay(.3f).SetEase(Ease.Linear);
        lose.transform.DOScale(Vector3.zero, .2f).SetDelay(.3f).SetEase(Ease.Linear);
        
        for (int i = 0; i < yuzde; i++)
        {
            yield return new WaitForSeconds(.01f);
            LoseComploted.GetComponent<CanvasGroup>().alpha += .025f;
        }
        
        for (int i = 0; i < yuzde; i++)
        {
            yield return new WaitForSeconds(.01f);
            LoseComploted.GetComponent<CanvasGroup>().alpha -= .025f;
        }
    }
}
