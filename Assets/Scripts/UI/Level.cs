
using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level : LocalSingleton<Level>
{
    
    public GameObject[] level;
    public GameObject[] levelComming;
    public GameObject[] levelup;
    public GameObject lvlFill;
    public Text lvlText;
    public int lvlIndex = 0;
    public int switchIndex;
    public int sI;
    bool scBool = false;
    
    void Awake()
    {
        lvlIndex = PlayerPrefs.GetInt("LevelIndex");
        lvlText = GameObject.Find("LevelText").GetComponent<Text>();
        switchIndex = PlayerPrefs.GetInt("Level");
        switchIndex = (switchIndex == 5) ? switchIndex = 0 : switchIndex ;
        for (int i = switchIndex; i <= level.Length-1; i++)
        {
            level[i].SetActive(true);
        }
        for (int i = 1; i < level.Length; i++)
        {
            levelComming[i].SetActive(false);
        }
        for (int i = 0; i < switchIndex; i++)
        {
            levelup[i].SetActive(true);
        }

    }

    private void Start()
    {
        
       //lvlIndex = (lvlIndex == 0) ?  : lvlIndex;
       if (lvlIndex ==0)
       {
           lvlIndex = 1;
       }
       else
       {
           lvlIndex = PlayerPrefs.GetInt("LevelIndex");
       }
    }

    void Update()
    {
        lvlText.text = "Level: " + lvlIndex.ToString();
        if (sI < switchIndex)
        {
            sI = switchIndex;
            LevelUp();
        }
    }
    void LevelUp()
    {
        switch (switchIndex)
        {
            case 0:
                LevelSwich();
                lvlFill.GetComponent<Image>().fillAmount = 0f;
                break;
            case 1:
                LevelSwich();
                StartCoroutine(FillValuve(25));
                break;
            case 2:
                LevelSwich();
                StartCoroutine(FillValuve(50));
                break;
            case 3:
                LevelSwich();
                StartCoroutine(FillValuve(75));
                break;
            case 4:
                LevelSwich();
                StartCoroutine(FillValuve(100));
                break;
        }
    }
    void LevelSwich()
    {
        if (switchIndex != 5)
        {
            if (switchIndex != 0)
            {
                level[switchIndex - 1].SetActive(false);
                levelup[switchIndex - 1].SetActive(true);
            }
            else
            {
                levelup[switchIndex].SetActive(false);
                level[switchIndex].SetActive(false);
            }
            levelComming[switchIndex].SetActive(true);
        }
        else
        {
            for (int i = 0; i < levelup.Length; i++)
            {
                levelup[i].SetActive(true);
            }
            for (int i = 0; i < levelup.Length; i++)
            {
                levelComming[i].SetActive(false);
            }
            for (int i = 0; i < levelup.Length; i++)
            {
                level[i].SetActive(false);
            }
        }
    }
    IEnumerator FillValuve(int amountValue)
    {
        for (int i = 0; i < amountValue; i++)
        {
            yield return new WaitForSeconds(.01f);
            lvlFill.GetComponent<Image>().fillAmount += .01f;
        }
        yield return new WaitForSeconds(.01f);
        if (switchIndex < 5)
            levelComming[switchIndex].transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), .2f).SetEase(Ease.OutBounce).OnComplete(() =>
            {
                levelComming[switchIndex].transform.DOScale(new Vector3(1f, 1f, 1f), .2f);
            });
    }
    

    public void TapToStart()
    {

        GameObject.Find("TapToStart").transform.DOPunchScale(new Vector3(.2f, .2f, .2f), .3f).OnComplete((() =>
        {
            GameObject.Find("TapToStart").transform.DOScale(Vector3.zero, .3f).SetEase(Ease.Linear).OnComplete(() =>
            {
                PlayerMover.onStart = true;
                transform.DOScale(Vector3.zero, .3f).SetEase(Ease.Linear);
            });

        }));
    }

    public void NextLevel()
    {
        StartCoroutine(NexLeleveRoutine());
    }
    public void RetyLevel()
    {
        StartCoroutine(NexLeleveRoutine(false));
    }
    IEnumerator NexLeleveRoutine(bool onLevel=true)
    {
        LevelComplotedLose.Instance.complotedB = true;
        yield return new WaitForSeconds(.5f);
        if (onLevel)
        {
            int levelIndex = SceneManager.GetActiveScene().buildIndex+1;
            int currentIndex = SceneManager.sceneCountInBuildSettings - 1;
            yield return new WaitForSeconds(.5f);
            if (levelIndex<=currentIndex)
            {
                switchIndex++;
                lvlIndex++;
                yield return new WaitForSeconds(.5f);
                SceneManager.LoadScene(levelIndex);
            }
            else
            {
                SceneManager.LoadScene(0);
            }
        }
        else
        { 
            yield return new WaitForSeconds(.5f);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    
}
